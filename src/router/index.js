import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { Login, Dashboard } from '../pages';

const Stack = createStackNavigator();

const Router = () => {
    return (
        <Stack.Navigator>
            {/* <Stack.Screen name="Splash" component={Splash} /> */}
            {/* <Stack.Screen name="Login" component={Login} options={{headerShown: false,}} /> */}
            <Stack.Screen name="Dashboard" component={Dashboard} options={{headerShown: false,}} />
        </Stack.Navigator>
    );
};

export default Router;