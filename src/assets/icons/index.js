import IconEye from './IconEye.svg';
import IconNotif from './IconNotif.svg';
import IconProduct from './IconProduct.svg';
import IconCustomer from './IconCustomer.svg';
import IconDashboard from './IconDashboard.svg';
import IconAnalytics from './IconAnalytics.svg';
import IconCupon from './IconCoupon.svg';

export { 
    IconEye, 
    IconNotif, 
    IconProduct, 
    IconCustomer, 
    IconDashboard, 
    IconAnalytics, 
    IconCupon,
}