const colors = {
    default: '#39A2DB',
    secondary: '#42BDA1',
    third: '#92929D',
    fourth: '#F04461',
};