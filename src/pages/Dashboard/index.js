import React from "react";
import { StyleSheet, View, Text, ScrollView } from "react-native";
import { Category, Head, MenuCard, NavigationBar, SearchingBar } from "../../components";

const Dashboard = () => {
    return (
        <View>
            <ScrollView>
                <Head />
                <SearchingBar />
                <Category />
                <Text style={styles.textFood}>Food</Text>
                {/* <View style={styles.cardContainer}>
                    <MenuCard />
                    <MenuCard />
                    <MenuCard />
                </View>
                <View style={styles.cardContainer}>
                    <MenuCard />
                    <MenuCard />
                    <MenuCard />
                </View>
                <Text style={styles.textdrink}>Drinks</Text>
                <View style={styles.cardContainer}>
                    <MenuCard />
                    <MenuCard />
                    <MenuCard />
                </View> */}
            </ScrollView>
            <NavigationBar />
        </View >
    )   
}

const styles = StyleSheet.create({
    textFood: {
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 15,
        marginBottom: 10,
        marginLeft: 10,
    },
    textdrink: {
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 30,
        marginBottom: 10,
        marginLeft: 10,
    },
    cardContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 10,
    }
})

export default Dashboard;