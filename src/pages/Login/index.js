import React from "react";
import { View, Text, StyleSheet, ScrollView, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { ILLogin } from '../../assets';
import { CheckBox, Button, Gap, Link, InputText } from '../../components';

const Login = () => {
    return ( 
      <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.page}>
        <Image source={ILLogin} style={styles.illustration} />
        <InputText label="Email" />
        <Gap height={23} />
        <InputText label="Password" secureTextEntry type="password" />
        <View style={styles.forgotPassword}>
          <CheckBox label="Ingat saya" />
          <Link text="Lupa Password?" />
        </View>
        <View style={styles.action}>
          <Button label="Sign In" />
          <Gap height={10} />
          <Text>Or</Text>
          <Gap height={10} />
          <Button label="Sign Up" color="#959595" />
        </View>
      </View>
      </ScrollView>
    );
  };

const styles = StyleSheet.create({
    page: {
      marginHorizontal: 20,
      marginTop: 45,
    },
    illustration: {
      alignSelf: "center",
    },
    forgotPassword: {
      flexDirection: "row",
      justifyContent: "space-between",
      marginVertical: 20,
    },
    action: {
      alignItems: "center",
      justifyContent: "center",
    },
  });

export default Login;