import React from "react";
import { StyleSheet, View, Text } from "react-native";

const NavigationBar = () => {
    return (
        <View>
            <View style={styles.containerNav}>
                <View style={styles.product}>

                </View>
                <View style={styles.customer}>

                </View>
                <View style={styles.dashboard}>

                </View>
                <View style={styles.analytics}>

                </View>
                <View style={styles.cupon}>

                </View>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    containerNav: {
        backgroundColor: 'white',
        width: 411,
        height: 50,
        flexDirection: 'row'
    },
    product: {
        backgroundColor: 'aqua',
        width: 60,
        height: 48,
        flex: 1
    },
    customer: {
        backgroundColor: 'gray',
        width: 60,
        height: 48,
        flex: 1
    },
    dashboard: {
        backgroundColor: 'blue',
        width: 60,
        height: 48,
        flex: 1
    },
    analytics: {
        backgroundColor: 'black',
        width: 60,
        height: 48,
        flex: 1
    },
    cupon: {
        backgroundColor: 'white',
        width: 60,
        height: 48,
        flex: 1
    },
})

export default NavigationBar;