import React, { useEffect } from "react";
import { StyleSheet, Text, View, FlatList, StatusBar } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { ILFood } from "../../../assets/illustrations/ILFood.png";

const MenuCard = () => {
    return (
        <View>
            <View style={styles.allCardContainer}>
                <View style={styles.containerCard}>
                    <View style={styles.img}></View>
                    <Text style={styles.text1}>Nasi Goreng</Text>
                    <Text style={styles.text2}>Rp. 20,000</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    containerCard: {
        backgroundColor: '#FFFFFF',
        width: 125,
        height: 125,
        padding: 8,
        borderRadius: 8,
    },
    img: {
        backgroundColor: 'black',
        width: 109,
        height: 69,
        marginBottom: 5,
    },
    text1: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    text2: {
        color: '#42BDA1',
        fontSize: 12,
        fontWeight: 'bold',
    }
})

export default MenuCard;