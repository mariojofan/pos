import React from "react";
import {TouchableOpacity} from "react-native";
import {IconBack, IconEye, IconMenuBooks, IconMinus, IconPlus, IconSandClock, IconHome, IconArrowRight, IconHumburger, IconNontification, IconSearch} from "../../../assets";

const IconOnly = ({ icon, onPress }) => {
  const Icon = () => {
    if (icon === "icon-eye") {
      return <IconEye />;
    }
    else if (icon === "icon-back") {
      return <IconBack onPress={() => {alert('Back')}} />
    }
    else if (icon === 'icon-arrow-right') {
      return <IconArrowRight onPress={() => {alert('Show')}} />
    }
    else if (icon === 'icon-minus') {
      return <IconMinus />
    }
    else if (icon === 'icon-plus') {
      return <IconPlus />
    }
    else if (icon === 'icon-home') {
      return <IconHome onPress={() => {alert('Home')}} />
    }
    else if (icon === 'icon-menu-books') {
      return <IconMenuBooks onPress={() => {alert('Menu')}} />
    }
    else if (icon === 'icon-sand-clock') {
      return <IconSandClock onPress={() => {alert('On Proccess')}} />
    }
    else if (icon === 'icon-humburger') {
      return <IconHumburger onPress={() => {alert('Setting')}} />
    }
    else if (icon === 'icon-nontification') {
      return <IconNontification onPress={() => {alert('Notif')}} />
    }
    else if (icon === 'icon-search') {
      return <IconSearch />
    }
    
   
    //tambah else if disini untuk icon lain
    return <IconEye />;
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <Icon />
    </TouchableOpacity>
  );
};

export default IconOnly;